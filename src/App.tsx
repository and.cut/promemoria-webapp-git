/* import logo from './logo.svg'; */
import React, {Component} from 'react';
import './App.css';
import {List} from "./models/List";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Lists from "./containers/Lists";
import ListDetail from "./containers/ListDetail";
import Reminders from "./containers/Reminders";
import ListController from "./controllers/ListController";

interface AppProps {
}

interface AppState {
    elenchi: List[];
}

export default class App extends Component<AppProps, AppState> {
    listcontroller: ListController = new ListController()

    constructor(props: AppProps) {
        super(props);
        this.state = {
            elenchi: this.listcontroller.getLists()
        };
    }

    render() {

        return <BrowserRouter>
            <Switch>
                <Route
                    path="/lists/:id/reminders"
                    render={(props) => {
                        const idElenco: string = props.match.params.id;
                        const elencoTrovato = this.listcontroller.getList(idElenco);
                        return <Reminders
                            elenco={elencoTrovato}
                            reminders={(elencoTrovato) ? elencoTrovato.reminders : undefined}/>
                    }}
                />
                <Route
                    path="/lists/:id"
                    render={(props) => {
                        const idElenco: string = props.match.params.id;
                        const isNew = idElenco === "new";
                        const elencoId = this.listcontroller.getList(idElenco);
                        return <ListDetail
                            isNew={isNew}
                            elenco={elencoId}
                            onCreate={(name, color, priority) => {
                                this.listcontroller.createList(name, priority, color)
                                this.setState({elenchi: this.listcontroller.getLists()});
                                props.history.push("/");
                            }}
                            onUpdate={(name, color, priority, id) => {
                                this.listcontroller.updateList(name, priority, color, id)
                                    this.setState({elenchi: this.listcontroller.getLists()});
                                    props.history.push("/");
                                }
                            }
                            onDelete={() => {
                                this.listcontroller.deleteList(idElenco)
                                this.setState({elenchi: this.listcontroller.getLists()});
                                props.history.push("/");
                            }}
                            {...props}/>
                    }}/>
                <Route
                    path="/"
                    render={(props) =>
                        <Lists elenchi={this.state.elenchi.sort(function (a, b) {
                            return a.priority - b.priority;
                        })} {...props} />}
                />
            </Switch>
        </BrowserRouter>
    }


}
/*LOGO APP
 <header className="App-header">
  <img src={logo} className="App-logo" alt="logo" />
  <p>
    Edit <code>src/App.tsx</code> and save to reload.
  </p>
  <a
      className="App-link"
      href="https://reactjs.org"
      target="_blank"
      rel="noopener noreferrer"
  >
    Learn React
  </a>
</header> */

/* PROVE

<h1>Lists</h1>
        <div>
            {elenchi.map((elenco, index) => {
                return <p><span className="dot" style={{backgroundColor: elenco.color}}></span> {elenco.name} {elenco.reminders.length}</p>
            })}
        </div>
 */

