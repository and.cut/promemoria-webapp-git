interface IReminder {
  id?: string;
  name: string;
  order?: number;
  description?: string;
  done?: boolean;
}

export class Reminder implements IReminder {
  id?: string;
  name: string;
  order?: number;
  description?: string;
  done?: boolean;

  constructor(obj: IReminder) {
    this.id = obj.id;
    this.name = obj.name;
    this.order = obj.order;
    this.description = obj.description;
    this.done = obj.done;
  }
}
