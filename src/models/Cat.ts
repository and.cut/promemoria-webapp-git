import {Animal} from "./Animal";
import {Eye} from "./Eye";



export class Cat implements Animal{
    eye: Eye[];
    legs: number = 4;
    race: string;
    color: string;

    constructor(color:string, eye:Eye[], race:string) {
        this.eye = eye;
        this.color = color;
        this.race = race;
    }

    breathe(): void {

    }

    eat(food: string): boolean {
        console.log(food);
        return true;
    }


}
