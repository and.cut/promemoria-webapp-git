export interface Animal {
    // Attributi
    legs: number;
    color: string;
    eye: any[]
    race: string;
    name?: string; //opzionale


    //Metodi
    eat: Function;
    breathe: Function;
    drink?: Function;
    walk?: Function; //opzionale
}