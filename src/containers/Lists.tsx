import React, {Component} from "react";
import {List} from "../models/List";
import Header from "../components/Header";
import {Link} from "react-router-dom";

interface ListsProps {
    elenchi: List[]
}


export default class Lists extends Component <ListsProps> {

    render() {

        return <>
            <Header title="Lists"/>
            {this.props.elenchi.map((elenco) => {
                return <Link to={"/lists/" + elenco.id + "/reminders"}>
                    <div className="box" key={elenco.id}>
                        <p>
                            <span className="dot" style={{backgroundColor: elenco.color}} />
                            <span>{elenco.name}</span>
                            <span>({elenco.reminders.length})</span>
                        </p>
                    </div>
                </Link>
            })}
            <Link to="/lists/new">
                <button>Nuovo elenco</button>
            </Link>
        </>
    }
}