import React, {Component, FormEvent} from "react";
import {List} from "../models/List";
import Header from "../components/Header";
import {Color} from "../models/Color.enum";


interface ListDetailProps {
    elenco?: List;
    isNew: boolean;
    onCreate: (name: string, color: Color, priority: number) => any;
    onUpdate: (name: string, color: Color, priority: number,id: string) => any;
    onDelete: (id: string) => any;
}

interface ListDetailState {
    name: string;
    color: Color;
    colors: Color[];
    priority?: number;
}

export default class ListDetail extends Component <ListDetailProps, ListDetailState> {
    constructor(props: ListDetailProps) {
        super(props);
        const colors: Color[] = Object.values(Color);
        this.state = {
            name: (props.elenco) ? props.elenco.name : "",
            priority: (props.elenco?.priority) ? props.elenco.priority : 0,
            color: (props.elenco) ? props.elenco.color : colors[0],
            colors: colors,
        }
    }

    onSave(e: FormEvent): void {
        e.preventDefault();
        if (this.props.elenco) {
            this.props.onUpdate(this.state.name, this.state.color, this.state.priority || 0, this.props.elenco.id)
        } else {
        this.props.onCreate(this.state.name, this.state.color, this.state.priority || 0);
    }}

    render() {
        return <>
            <Header title={(this.props.isNew) ? "Nuova lista" : "Modifica lista"}/>
            <form onSubmit={(ev: FormEvent) => this.onSave(ev)}>
                <div className="container list-detail-container">
                    <div className="form-input">
                        <label>Nome elenco</label>
                        <input type="text"
                               value={this.state.name}
                               onChange={(event) => this.setState({name: event.target.value})}/>
                    </div>
                    <div className="form-input">
                        <label>Priorità</label>
                        <input type="text"
                               value={this.state.priority}
                               onChange={(event) => this.setState({priority: isNaN(+event.target.value) ? this.state.priority : +event.target.value})}/>
                    </div>
                    <div className="form-input">
                        <label>Colore</label>
                        <div className="colors">
                            {this.state.colors.map((c) => <div
                                key={c}
                                className={"color" + ((this.state.color === c) ? " active" : "")}
                                style={{backgroundColor: c}}
                                onClick={(event) => this.setState({color: c})}
                            >
                            </div>)}
                        </div>
                    </div>
                </div>
                    <button type="button"
                            className="btn btn-danger"
                            onClick={(event) => this.props.elenco
                                && !this.props.isNew
                                && this.props.onDelete(this.props.elenco.id)}
                    >Elimina
                    </button>
                    <button
                        className="btn btn-primary"
                        type="submit"
                        disabled={this.state.name.trim().length <= 0}

                    >Salva
                    </button>

            </form>
        </>

    }

}