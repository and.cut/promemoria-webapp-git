import React from "react";
import {Reminder} from "../models/Reminder";
import {Link} from "react-router-dom";
import {List} from "../models/List";
import Header from "../components/Header";

interface ReminderProps {
    reminders?: Reminder[];
    elenco?: List;
}

export default function Reminders(props: ReminderProps) {
    return <> <Header title="Reminders"/>
        {(!props.reminders || props.reminders.length === 0)
                ? <p>Questo elenco non contiene promemoria</p>
                : props.reminders.map((rem) => {
                    return <p key={rem.id}>{rem.name}</p>;
        })}
        {(props.elenco) ?<Link to={"/lists/" + props.elenco.id}><button>Modifica elenco</button></Link> : undefined}
    </>

}