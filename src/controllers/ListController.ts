import {List} from "../models/List";
import {Color} from "../models/Color.enum";
import {Reminder} from "../models/Reminder";



export default class ListController {
    private BASE_URL = "https://firestore.googleapis.com/v1/projects/promemoria-4dfa6/database/(default)/documents/"
    private elenchi: List[];

    constructor() {
        this.elenchi = [
            new List("Primo elenco", 2, Color.Red, [new Reminder({name: "Test1"})], "0001"),
            new List("Secondo elenco", 1, Color.Orange, [new Reminder({name: "Test2"})], "0002"),
            new List("Terzo elenco", 0, Color.Pink, [new Reminder({name: "Test3"})], "0003"),
        ]
    }

    getLists(): List[] {
        return this.elenchi;
    }

    getList(id: string): List | undefined {
        return this.elenchi.find((el) => el.id === id);
    }

    createList(name: string, priority: number, color: Color, id?: string, reminders?: Reminder[]): void {
        const list: List = new List(name, priority, color, reminders, id);
        this.elenchi.push(list);
    }

    updateList(name: string, priority: number, color: Color, id?: string, reminders?: Reminder[]): void {
        const indice: number = this.elenchi.findIndex((el) => el.id === id);
        if (indice >= 0) {
            this.elenchi[indice].name = name;
            this.elenchi[indice].color = color;
            this.elenchi[indice].priority = priority;
            if (reminders){
            this.elenchi[indice].reminders = reminders;
            }
        } else {
            throw new Error("Elenco non trovato");
        }
    }

    deleteList(id: string): void {
        this.elenchi = this.elenchi.filter((el) => el.id !== id);
    }

    deleteAll(id: string): void {
        this.elenchi = [];
    }

}